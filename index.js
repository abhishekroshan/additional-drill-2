const got = require("./data-1");
/*-------------------------------------------------------------------------------------------- */

function countAllPeople(obj) {
  try {
    const result = Object.values(obj);
    //getting the values in the result array;

    let totalPeople = 0;
    //initialize the count with 0.

    const res = result.forEach((value) => {
      //iterate through the values of result
      value.forEach((item) => {
        /*
        iterate inside each value and each item and add its 
        people length to the totalpeople variable 
      */
        totalPeople += item.people.length;
      });
    });

    return totalPeople;
  } catch (error) {
    console.log(error);
  }
}

//console.log(countAllPeople(got));

/*---------------------------------------------------------------------------------------------- */

function peopleByHouses(obj) {
  try {
    const result = Object.values(obj).reduce((accumulator, currentValue) => {
      //storing the values in the result variable and applying reduce method
      currentValue.forEach((item) => {
        //applying foreach for the currentValue in the array
        const houseName = item.name;
        //stroing the name of the house

        accumulator[houseName] = item.people.length;
        //creating a key with the name of the house and assigning its length
        //the length of the people array.
      });
      return accumulator;
    }, {});
    return result;
  } catch (error) {
    console.log(error);
  }
}

//console.log(peopleByHouses(got));

/*----------------------------------------------------------------------------------------------------- */

function everyone(obj) {
  try {
    const result = Object.values(obj).reduce((accumulator, currentValue) => {
      //storing the values in the result and applying reduce with an empty array
      currentValue.forEach((value) => {
        //in the current value iterating through each element
        const peoples = value.people;
        //storing the people name in the peoples variable
        peoples.forEach((item) => {
          //iterating through the peoples and pushing the name of every index in the array
          accumulator.push(item.name);
        });
      });

      return accumulator;
    }, []);
    return result;
  } catch (error) {
    console.log(error);
  }
}

//console.log(everyone(got));

/*------------------------------------------------------------------------------------------------------- */

function nameWithS(obj) {
  try {
    const result = Object.values(obj).reduce((accumulator, currentValue) => {
      //storing the values in the result and applying reduce with an empty array
      currentValue.forEach((value) => {
        //in the current value iterating through each element
        const peoples = value.people;
        //storing the people name in the peoples variable

        peoples.forEach((item) => {
          /*Iterating through the people and if the name includes
            s or S we push the value in the array */
          if (item.name.includes("s") || item.name.includes("S")) {
            accumulator.push(item.name);
          }
        });
      });
      return accumulator;
    }, []);
    return result;
  } catch (error) {
    console.log(error);
  }
}

//console.log(nameWithS(got));

/*----------------------------------------------------------------------------------------------------------------------- */

function nameWithA(obj) {
  try {
    const result = Object.values(obj).reduce((accumulator, currentValue) => {
      //storing the values in the result and applying reduce with an empty array
      currentValue.forEach((value) => {
        //in the current value iterating through each element
        const peoples = value.people;
        //storing the people name in the peoples variable

        peoples.forEach((item) => {
          /*Iterating through the people and if the name includes
              a or A we push the value in the array */
          if (item.name.includes("a") || item.name.includes("A")) {
            accumulator.push(item.name);
          }
        });
      });
      return accumulator;
    }, []);
    return result;
  } catch (error) {
    console.log(error);
  }
}

//console.log(nameWithA(got));

/*----------------------------------------------------------------------------------------------------------------------- */

function surnameWithS(obj) {
  try {
    const result = Object.values(obj).reduce((accumulator, currentValue) => {
      //storing the values in the result and applying reduce with an empty array
      currentValue.forEach((value) => {
        //in the current value iterating through each element
        const peoples = value.people;
        //storing the people name in the peoples variable

        peoples.forEach((item) => {
          const name = item.name;
          //getting the name
          const nameArray = name.split(" ");
          //splitting the name based on the space
          const nameLength = nameArray.length;
          //getting the length of the name

          const surName = nameArray[nameLength - 1];
          //getting the last item in the name

          if (surName.charAt(0) === "S") {
            //checking the character at 0th index in the surname
            accumulator.push(name);
            //pushing the name in the array
          }
        });
      });
      return accumulator;
    }, []);
    return result;
  } catch (error) {
    console.log(error);
  }
}

//console.log(surnameWithS(got));

/*--------------------------------------------------------------------------------------------------------- */

function surnameWithA(obj) {
  try {
    const result = Object.values(obj).reduce((accumulator, currentValue) => {
      //storing the values in the result and applying reduce with an empty array
      currentValue.forEach((value) => {
        //in the current value iterating through each element
        const peoples = value.people;
        //storing the people name in the peoples variable

        peoples.forEach((item) => {
          const name = item.name;
          //getting the name
          const nameArray = name.split(" ");
          //splitting the name based on the space
          const nameLength = nameArray.length;
          //getting the length of the name

          const surName = nameArray[nameLength - 1];
          //getting the last item in the name

          if (surName.charAt(0) === "A") {
            //checking the character at 0th index in the surname
            accumulator.push(name);
            //pushing the name in the array
          }
        });
      });
      return accumulator;
    }, []);
    return result;
  } catch (error) {
    console.log(error);
  }
}

//console.log(surnameWithA(got));

/*----------------------------------------------------------------------------------------------- */

function peopleNameOfAllHouses(obj) {
  try {
    const result = Object.values(obj).reduce((accumulator, currentValue) => {
      //storing the values in the result and applying reduce with an empty array
      currentValue.forEach((value) => {
        const house = value.name;
        //getting the name of the house
        const peopleArray = value.people;
        //getting the all the peoples in that house

        if (!accumulator[house]) {
          accumulator[house] = [];
        }
        //creating a empty array with house-name

        peopleArray.forEach((item) => {
          const people = item.name;
          //getting the name of the person in the house in the variable people

          accumulator[house].push(people);
          //pushing the element in the corrosponding house
        });
      });
      return accumulator;
    }, {});
    return result;
  } catch (error) {
    console.log(error);
  }
}

//console.log(peopleNameOfAllHouses(got));
